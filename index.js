import express from 'express';

import { people, planets } from './swapi_cache.js'

const app = express();

function handle_response(res, data) {
    if (data) {
        res.status(200).json({
            count: data.length,
            results: data        
        })
    }
    else {
        res.status(404)
    }
}

// Endpoints
app.get('/', (req, res, next) => {
    return res.send('SWAPI Node Exercise')
});

app.get('/people', (req, res, next) => {
    const param = req.query.sortBy
    // Sort people according to sortBy query parameter.
    if (people[0][param]) {
        let sorted_people = parseInt(people[0][param])
            // Sort numeric values and move unkonwn values to the end.
            ? people.sort((a, b) => (a[param]==='unknown')-(b[param]==='unknown') 
            // Disregard commas on numeric values.
            || parseFloat(a[param].replace(/,/g, ''))
            - parseFloat(b[param].replace(/,/g, '')))
            // Sort strings.
            : people.sort((a, b) => a[param].localeCompare(b[param]));
        handle_response(res, sorted_people)
    }
    else {
        handle_response(res, people)
    }
});

app.get('/planets', (req, res, next) => {
    handle_response(res, planets)
});

// Listen for API requests.
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});
