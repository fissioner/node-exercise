import fetch from 'node-fetch';

async function fetch_all_pages(url) {
    const all_page_data = await fetch_page(url + '?page=1', [])
    return all_page_data
};

async function fetch_page(url, results) {
    try {
        const response = await fetch(url);
        var data = await response.json();
    } catch (error) {
        console.log(error);
    }
    results.push(...data.results)
    if (data.next){
        return fetch_page(data.next, results);
    }
    return results
};

// Fetch all people and planet data.
console.log('Caching SWAPI data...')
const swapi_url = 'https://swapi.dev/api/'
const all_people = await fetch_all_pages(swapi_url + 'people')
let all_planets = await fetch_all_pages(swapi_url + 'planets')

// Populate planet data with resident names.
const people_names = {}
all_people.forEach(person => {
    const id = person.url.match(/\d/g).join('');
    people_names[id] = person.name
});
all_planets.forEach(planet => {
    let residents = []
    planet.residents.forEach(resident => {
        const id = resident.match(/\d/g).join('');
        residents.push(people_names[id])
    })
    planet.residents = residents
})
console.log('Caching complete ^_^\n')

export const people = all_people
export const planets = all_planets